def measure(number_of_times = 1, &prc)
  start_time = Time.now
  number_of_times.times {prc.call}
  end_time = Time.now
  (end_time - start_time) / number_of_times
end
