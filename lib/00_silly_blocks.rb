def reverser(&prc)
  sentence = prc.call
  words = sentence.split
  words_array = []
  words.each do |word|
    words_array << word.reverse
  end
  words_array.join(" ")
end

def adder(additive = 1, &prc)
  base_number = prc.call
  base_number + additive
end

def repeater(num_of_times = 1, &prc)
  num_of_times.times {prc.call}
end
